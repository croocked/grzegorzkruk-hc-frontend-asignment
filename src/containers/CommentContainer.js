import React, { Component } from 'react';
import { connect } from 'react-redux';

import { PropTypes } from 'prop-types';
import Comment from '../components/Comment/Comment';

class CommentContainer extends Component {
    render() {
        return (
            <Comment
                title={'Comment'}
                comment={this.props.comment}
                user={this.props.user}
                editMode={this.props.editMode}
                onSubmit={this.props.onCommentSubmit}
                onSwitchEditMode={this.props.onSwitchEditMode}/>
        );
    }
}

CommentContainer.propTypes = {
    relatedReview: PropTypes.any,
    user: PropTypes.any,
    comment: PropTypes.string,
    editMode: PropTypes.bool,
    onCommentSubmit: PropTypes.func,
    onSwitchEditMode: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: ownProps.editMode ? state.user.data : ownProps.user,
    };
};

export default connect(mapStateToProps)(CommentContainer);