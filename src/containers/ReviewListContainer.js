import React, { Component } from 'react';
import { connect } from 'react-redux';

import ReviewCard from '../components/ReviewCard/ReviewCard';

import reviewActions from '../store/actions/reviewActions';

import { PropTypes } from 'prop-types';
import HcCard from '../components/HcCard/HcCard';

class ReviewListContainer extends Component {
    componentDidMount () {
        this.props.fetchReviews();
    }
    render() {
        if (this.props.reviews.length === 0) {
            return (
                <HcCard
                    style={{
                        margin: '100px 0',
                        padding: '100px',
                        textAlign: 'center',
                    }}>
                    <div>No data</div>
                </HcCard>
            );
        }
        return this.props.reviews.map((review) =>
            <ReviewCard
                key={review.id}
                creationDate={review.creationDate}
                title={review.title}
                content={review.content}
                relatedComment={review.comment}
                rating={review.rating}
                liked={review.liked}
                authorImage={review.author.picture}
                authorName={review.author.name}
                onCommentSubmit={(comment, user) => this.props.addComment(review, comment, this.props.userData)}
            />
        );
    }
}

ReviewListContainer.propTypes = {
    reviews: PropTypes.array,
    fetchReviews: PropTypes.func,
    addComment: PropTypes.func,
    fetchUser: PropTypes.func,
    userData: PropTypes.any,
};

const mapStateToProps = (state) => {
    return {
        reviews: state.reviews.items,
        loading: state.reviews.loading,
        userData: state.user.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchReviews: () => dispatch(reviewActions.fetchReviews()),
        addComment: (relatedReview, commentMessage, user) => dispatch(reviewActions.addComment(relatedReview, commentMessage, user)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewListContainer);