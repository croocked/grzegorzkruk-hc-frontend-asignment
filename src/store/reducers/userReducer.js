import * as actionTypes from '../actions/actionTypes';

const initialState = {
    data: {},
    loading: false,
};

const fetchUserStart = ( state ) => {
    return {
        ...state,
        ...{
            loading: true,
        }
    };
};


const fetchUserSuccess = ( state, action ) => {
    return {
        ...state,
        ...{
            data: action.user,
            loading: false,
        }
    };
};

const fetchUserFail = ( state ) => {
    return {
        ...state,
        ...{ loading: false }
    };
};

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.FETCH_USER_START: return fetchUserStart(state, action);
        case actionTypes.FETCH_USER_SUCCESS: return fetchUserSuccess(state, action);
        case actionTypes.FETCH_USER_FAIL: return fetchUserFail(state, action);
        default: return state;
    }
};

export default reducer;