import * as actionTypes from '../actions/actionTypes';

const initialState = {
    items: [],
    loading: false,
};

const fetchReviewsStart = ( state ) => {
    return {
        ...state,
        ...{
            loading: true,
        }
    };
};


const fetchReviewsSuccess = ( state, action ) => {
    return {
        ...state,
        ...{
            items: state.items.concat(action.items),
            loading: false,
        }
    };
};

const fetchReviewsFail = ( state ) => {
    return {
        ...state,
        ...{ loading: false }
    };
};

const addComment = ( state, action ) => {
    const { relatedReview, commentMessage } = action;

    const newArray = [...state.items];

    const editItemIndex = newArray.findIndex((item) => item.id === relatedReview.id);
    newArray[editItemIndex]['comment']={
        author: action.user,
        content: commentMessage,
        creationTime: new Date(),
    };
    
    return {
        ...state,
        ...{
            items: [...newArray]
        }
    };
};

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.FETCH_REVIEWS_START: return fetchReviewsStart(state, action);
        case actionTypes.FETCH_REVIEWS_SUCCESS: return fetchReviewsSuccess(state, action);
        case actionTypes.FETCH_REVIEWS_FAIL: return fetchReviewsFail(state, action);
        case actionTypes.ADD_COMMENT: return addComment(state, action);
        default: return state;
    }
};

export default reducer;