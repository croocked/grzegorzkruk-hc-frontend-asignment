import UserModel from '../../models/UserModel';

export default class RandomUserSource {
    static url = 'https://randomuser.me/api/';
    static fetch = (results) => {
        return fetch(`${RandomUserSource.url}?results=${results}&inc=name,picture`)
        .then((response) => response.json())
        .then((responseJSON) => {
            return new Promise((resolve, reject) => {
                const users = responseJSON.results.map((resItem) => {
                    const name = `${resItem.name.title} ${resItem.name.first} ${resItem.name.last}`;
                    const picture = resItem.picture.medium;
                    return new UserModel(name, picture);
                });
                if (users.length > 0){
                    resolve(users);
                } else {
                    reject();
                }
            });
        });
    }
}