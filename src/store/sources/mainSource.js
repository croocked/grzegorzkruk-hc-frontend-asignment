import axios from 'axios';

const mainSource = axios.create({
    baseURL: 'https://hc-frontend-assignment.firebaseio.com/'
});

export default mainSource;