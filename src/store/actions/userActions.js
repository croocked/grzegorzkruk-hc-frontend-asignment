import * as actionTypes from './actionTypes';
import RandomUserSource from '../sources/randomUserSource';
import UserModel from '../../models/UserModel';

const fetchUserStart = () => {
    return {
        type: actionTypes.FETCH_USER_START,
    };
};

const fetchUserSuccess= (user) => {
    return {
        type: actionTypes.FETCH_USER_SUCCESS,
        user,
    };
};

const fetchUserFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_FAIL,
        error,
    };
};

const fetchUser = () => {
    return (dispatch) => {
        dispatch(fetchUserStart());
        RandomUserSource.fetch(1)
            .then((data) => {
                if (data.length > 0 && data[0] instanceof UserModel){
                    data[0].role = 'Hotel Manager';
                    dispatch(fetchUserSuccess(data[0]));
                }
            })
            .catch((error) => {
                dispatch(fetchUserFail(error));
            });
    };
};

export default {
    fetchUserStart,
    fetchUserSuccess,
    fetchUserFail,
    fetchUser,
};