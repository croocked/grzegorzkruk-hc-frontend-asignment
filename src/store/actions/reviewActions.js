import * as actionTypes from './actionTypes';
import mainSource from '../sources/mainSource';
import RandomUserSource from '../sources/randomUserSource';

const fetchReviewsStart = () => {
    return {
        type: actionTypes.FETCH_REVIEWS_START,
    };
};

const fetchReviewsSuccess= (items) => {
    return {
        type: actionTypes.FETCH_REVIEWS_SUCCESS,
        items,
    };
};

const fetchReviewsFail = (error) => {
    return {
        type: actionTypes.FETCH_REVIEWS_FAIL,
        error,
    };
};

const fetchReviews = () => {
    return (dispatch) => {
        dispatch(fetchReviewsStart());
        mainSource.get('/reviews.json')
            .then((response) => {
                const fetchedReviews = [];
                Object.keys(response.data).forEach((key) => {
                    fetchedReviews.push({
                        ...response.data[key],
                        id: key,
                    });
                });
                if (fetchedReviews.length > 0) {
                    return new Promise((resolve, reject) => {
                        RandomUserSource.fetch(fetchedReviews.length)
                            .then((users) => {
                                users.forEach((user, i) => {
                                    fetchedReviews[i]['author'] = {
                                        name: user.name,
                                        picture: user.picture,
                                    };
                                });
                                dispatch(fetchReviewsSuccess(fetchedReviews));
                            });
                    });
                } else {
                    return new Promise((resolve, reject) => reject());
                }
            })
            .catch((error) => {
                dispatch(fetchReviewsFail(error));
            });
    };
};

const addComment = (relatedReview, commentMessage, user) => {
    return {
        type: actionTypes.ADD_COMMENT,
        relatedReview,
        commentMessage,
        user,
    };
};

export default {
    fetchReviewsStart,
    fetchReviewsSuccess,
    fetchReviewsFail,
    fetchReviews,
    addComment,
};