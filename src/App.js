import React, { Component } from 'react';

import './App.css';

import HcView from './components/HcView/HcView';
import ReviewListContainer from './containers/ReviewListContainer';

class App extends Component {
    render() {
        const  headerBg = require('./assets/images/background.jpg');
        return (
            <HcView
            headerBg={headerBg}>
                <ReviewListContainer/>
            </HcView>
        );
    }
}

export default App;
