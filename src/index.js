import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/fontello/css/fontello.css';
import './assets/fontello/css/fontello-codes.css';
import './assets/fontello/css//animation.css';
import './assets/fontello/css/fontello-ie7.css';
import './assets/fontello/css/fontello-ie7-codes.css';
import './assets/colors.css';
import './assets/variables.css';
import './index.css';

import App from './App';
import registerServiceWorker from './registerServiceWorker';


import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import reviewReducer from './store/reducers/reviewReducer';
import userReducer from './store/reducers/userReducer';

import userActions from './store/actions/userActions';

const rootReducer = combineReducers({
    reviews: reviewReducer,
    user: userReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

store.dispatch(userActions.fetchUser());


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('root'));
registerServiceWorker();
