export default class UserModel {
    name;
    picture;
    role;
    constructor(name, picture, role) {
        this.name = name;
        this.picture = picture;
        this.role = role;
    }
}