import React, { Component } from 'react';
import HcHeader from '../HcHeader/HcHeader';
import HcMenu from '../HcMenu/HcMenu';
import HcLogo from '../HcLogo/HcLogo';
import HcNavBar from '../HcNavBar/HcNavBar';
import HcContainer from '../HcContainer/HcConatiner';
import { PropTypes } from 'prop-types';

import './HcView.css';

export default class HcView extends Component {
    state={
        menuSticked: false,
    }

    onSize = (size) => {
        this.setState({
            measuredWidth: size.width
        });
    }

    render() {
        const  { headerBg } = this.props;
        const { menuSticked } = this.state;
        return(
            <div className="hc-view">
                <HcNavBar
                    isSticky={false}
                    measuredWidth={this.state.measuredWidth}
                    onSticked={this.onSticked}>
                    <HcLogo style={{margin: '0 30px'}} color1={menuSticked ? 'white' : 'rgb(35, 83, 160)'}/>
                    <HcMenu buttonLabelStyle={menuSticked ? {color: 'white'} : {}  }/>
                </HcNavBar>
                <HcHeader bgImage={headerBg}/>
                <HcContainer
                    onSize={this.onSize}
                    className="hc-view__container">
                    {this.renderContent()}
                </HcContainer>
            </div>
        );
    }

    renderContent() {
        return(
            <div className="hc-view__content-wrapper">
                <div className="hc-view__content">
                    {this.props.children}
                </div>
            </div>
        );
    }

    onSticked = (isSticked) => {
        this.setState({
            menuSticked: isSticked,
        });
    }
}

HcView.propTypes = {
    headerBg: PropTypes.any,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
};