import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './CommentForm.css';

export default class CommentForm extends Component {

    constructor(props) {
        super(props);
        this.state={
            value: this.props.valueToEdit,
        };
    }
    render() {
        return(
            <form className="hc-comment-form" onSubmit={this.onSubmit}>
                <textarea
                    className="hc-comment-form__input"
                    placeholder="Write your comment"
                    rows="4"
                    value={this.state.value}
                    onChange={this.onChange}
                />
                <input className="hc-comment-form__submit" type="submit" value="Submit"/>
            </form>
        );
    }

    onChange = (event) => {
        this.setState({
            value: event.target.value,
        });
    }

    onSubmit = (event) => {
        event.preventDefault();
        if (this.state.value && this.state.value !== ''){
            const { onSubmit } = this.props;
            onSubmit && onSubmit(this.state.value, event);
        }
    }
}


CommentForm.propTypes = {
    valueToEdit: PropTypes.string,
    onSubmit: PropTypes.func,
};