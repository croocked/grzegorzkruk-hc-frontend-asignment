import React, { Component } from 'react';
import './HcMenu.css';
import { PropTypes } from 'prop-types';

const MenuButton = (props) => {
    const { active, label, onClick, activeStyle, style, labelStyle, activeLabelStyle} = props;

    let containerClasses = ['hc-menu-button'];
    if (active) {
        containerClasses.push('hc-menu-button--active');
    }

    return (
        <div
        className={containerClasses.join(' ')}
        style={(active) ? activeStyle : style}
        onClick={onClick}>
            <span className="hc-menu-button__label" style={(active) ? activeLabelStyle : labelStyle}>{label}</span>
        </div>
    );
};

MenuButton.propTypes = {
    active: PropTypes.bool,
    label: PropTypes.string,
    onClick: PropTypes.func,
    style: PropTypes.any,
    activeStyle: PropTypes.any,
    labelStyle: PropTypes.any,
    activeLabelStyle: PropTypes.any,
};

MenuButton.defaultProps = {
    style: {},
    activeStyle: {},
    labelStyle: {},
};

export default class HcMenu extends Component {
    render() {
        return (
            <div className="hc-menu">
                {this.props.routes.map((route, index) => 
                    <MenuButton
                    style={this.props.buttonStyle}
                    activeStyle={this.props.buttonActiveStyle}
                    labelStyle={this.props.buttonLabelStyle}
                    activeLabelStyle={this.props.buttonActiveLabelStyle}
                    key={route.name + index}
                    label={route.name}
                    active={route.active}
                    onClick={() => { return; }}
                    />
                )}
            </div>
        );
    }
}

HcMenu.propTypes = {
    routes: PropTypes.array,
    buttonStyle: PropTypes.any,
    buttonActiveStyle: PropTypes.any,
    buttonLabelStyle: PropTypes.any,
    buttonActiveLabelStyle: PropTypes.any,
};

HcMenu.defaultProps = {
    routes: [
        {
            name: 'Dashboard',
            active: false,
        },
        {
            name: 'Reviews',
            active: true,
        },
        {
            name: 'Hotel Manager',
            active: false,
        },
        {
            name: 'Settings',
            active: false,
        }
    ],
    buttonStyle: {},
    buttonActiveStyle: {},
    buttonLabelStyle: {},
    buttonActiveLabelStyle: {},
};