import React, { Component } from 'react';
import './Rating.css';
import { PropTypes } from 'prop-types';

export default class Rating extends Component {
    render() {
        if (this.props.rate < 0 && this.props.rate > this.props.range) {
            return (<span>Out of range</span>);
        }
        return(
            <div className={'hc-rating'}>
                {
                    Array.from(new Array(this.props.range)).map((value, index) =>
                        (
                            <i
                            key={index}
                            className={`hc-rating__icon icon-star ${ (index < this.props.rate) ? 'hc-rating__icon--active' : '' }`}
                            onClick={(event) => this.props.onRateSelected(index + 1, event)}
                            />
                        )
                    )
                }
                <span className={'hc-rating__label'}>{this.props.rate}/{this.props.range}</span>
            </div>
        );
    }
}

Rating.propTypes = {
    range: PropTypes.number,
    rate: PropTypes.number,
    onRateSelected: PropTypes.func,
};

Rating.defaultProps = {
    range: 6,
    rate: 0,
    onRateSelected: () => { return; },
};