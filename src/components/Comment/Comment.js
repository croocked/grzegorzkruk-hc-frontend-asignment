import React, { Component } from 'react';
import PropTypes from 'prop-types';

import UserAvatar from '../UserAvatar/UserAvatar';
import CommentForm from '../CommentForm/CommentForm';
import CollapsibleContainer from '../CollapsibleContainer/CollapsibleContainer';

import './Comment.css';

export default class Comment extends Component {

    render() {
        return (
            <div className="hc-comment">
                <div className="hc-comment__body">
                    {this.props.editMode ? this.renderCommentForm() : this.renderCommentDetails()}
                </div>
                <div className="hc-comment__footer">
                    <UserAvatar
                        imageStyle={{ border: 'none' }}
                        titleStyle={{ color: 'black' }}
                        descriptionStyle={{ color: 'black' }}
                        imgSrc={this.props.user.picture}
                        name={this.props.user.name}
                        description={this.props.user.role}
                        label={'Add comment'}
                    />
                </div>
            </div>
        );
    }

    renderCommentDetails() {
        return(
            <div onDoubleClick={this.switchToEditMode}>
                <div className="hc-comment__title">
                    <span>Comment</span>
                </div>
                <div className="hc-comment__desc">
                    <CollapsibleContainer>
                        <p>{this.props.comment}</p>
                    </CollapsibleContainer>
                </div>
            </div>
        );
    }

    renderCommentForm() {
        return(
            <CommentForm
            valueToEdit={this.props.comment}
            onSubmit={this.onCommentSubmitted}/>
        );
    }

    onCommentSubmitted = (value, event) => {
        const { onSubmit, user } = this.props;
        onSubmit && onSubmit(value, user);
    }

    switchToEditMode = () => {
        const { onSwitchEditMode } = this.props;
        onSwitchEditMode && onSwitchEditMode();
    }
}

Comment.propTypes = {
    comment: PropTypes.string,
    user: PropTypes.any,
    editMode: PropTypes.bool,
    onSwitchEditMode: PropTypes.func,
    onSubmit: PropTypes.func,
};

Comment.defaultProps = {
    editMode: false,
};