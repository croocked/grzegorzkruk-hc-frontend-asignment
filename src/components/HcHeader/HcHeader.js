import React, { Component } from 'react';
import './HcHeader.css';
import { PropTypes } from 'prop-types';

export default class HcHeader extends Component {
    render() {
        const containerStyle = {
            backgroundImage: `url(${this.props.bgImage})`
        };

        return (
            <div
            className="hc-header"
            style={containerStyle}>
                {this.props.children}
            </div>
        );
    }
}

HcHeader.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    bgImage: PropTypes.string,
};