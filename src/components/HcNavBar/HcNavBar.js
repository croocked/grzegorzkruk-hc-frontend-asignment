import React, { Component } from 'react';
import './HcNavBar.css';
import { PropTypes } from 'prop-types';

export default class HcNavBar extends Component {
    measuredWidth = null;
    navBarContainer = {};
    state = {
        sticked: false,
        position: 'relative',
        width: '100%',
        transition: 'none',
    }

    constructor(props){
        super(props);
        this.navBarContainer = React.createRef();
    }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll);
        this.setState({
            width: `${this.props.measuredWidth}px`,
        });

        setTimeout(() => {
            this.setState({
                transition: 'width 0.5s, background-color 0.5s',
            });
        }, 500);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.measuredWidth !== nextProps.measuredWidth) {
            this.setState({
                width: `${nextProps.measuredWidth}px`,
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    }

    render() {
        const containerStyle = {
            position: (this.state.sticked) ? 'fixed' : 'absolute',
            top: (this.state.sticked) ? 0 : this.props.distanceTop,
            width: (this.state.sticked) ? '100%' : this.state.width,
            transition: this.state.transition,
            backgroundColor: (this.state.sticked) ? 'rgb(35, 83, 160)' : 'rgb(255, 255, 255)',
        };

        let containerClasses = ['hc-navbar'];
        (this.state.sticked) && containerClasses.push('hc-navbar--sticky');

        return (
            <div
            ref={this.navBarContainer}
            style={containerStyle}
            className={containerClasses.join(' ')}>
                <div
                    className="hc-navbar__content"
                    style={{
                        width: this.state.width,
                        height: (this.state.sticked) ? '4rem' : '8rem',
                    }}
                >
                    {this.props.children}
                </div>
            </div>
        );
    }

    onScroll = (event) => {
        const { scrollTop } = event.target.scrollingElement;
        const { distanceTop, onSticked } = this.props;
        if (distanceTop && typeof distanceTop === 'number') {

            this.measuredWidth = this.navBarContainer.current.clientWidth;
            const sticked = scrollTop >= distanceTop;
            this.setState({
                sticked
            });
            onSticked && onSticked(sticked);
        }
    }
}

HcNavBar.propTypes = {
    distanceTop: PropTypes.number,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    onSticked: PropTypes.func,
    measuredWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

HcNavBar.defaultProps = {
    distanceTop: 50,
};