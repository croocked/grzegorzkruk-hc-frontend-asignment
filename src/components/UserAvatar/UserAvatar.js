import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './UserAvatar.css';

export default class HcUserAvatar extends Component {
    render() {
        return(
            <div className="hc-user-avatar" style={this.props.style}>
                <img className="hc-user-avatar__image" style={this.props.imageStyle} src={this.props.imgSrc} alt="user-img"/>
                <span className="hc-user-avatar__title" style={this.props.titleStyle} >{this.props.name}</span>
                <span className="hc-user-avatar__description" style={this.props.descriptionStyle} >{this.props.description}</span>
            </div>
        );
    }
}

HcUserAvatar.propTypes = {
    imgSrc: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
};