import React, { Component } from 'react';

import './CollapsibleContainer.css';
import { PropTypes } from 'prop-types';

export default class CollapsibleContainer extends Component {
    contentRef = {};
    constructor(props) {
        super(props);
        this.contentRef = React.createRef();
        this.state={
            collapsed: true,
        };
    }

    componentDidMount() {
        if (this.contentRef) {
            const { minHeight } = this.props;
            const measuredHeight = this.contentRef.current.clientHeight;
            this.setState({
                measuredHeight: measuredHeight,
                collapsed: (measuredHeight > minHeight),
            });
        }
    }

    render() {
        const { collapsed, measuredHeight } = this.state;
        const { minHeight } = this.props;

        let containerClasses = ['hc-expandable__text-box'];
        (collapsed) && containerClasses.push('hc-expandable__text-box--collapsed');

        return(
            <div
            className={containerClasses.join(' ')}
            onClick={this.toggle}
            style={
                {
                    height: ((measuredHeight > minHeight) && collapsed) ?
                        `${minHeight}px` : `${measuredHeight}px`,
                }
            }>
                <div
                    style={this.props.decorStyle}
                    className="hc-expandable__decor"
                />
                <div ref={this.contentRef}>
                    {this.props.children}
                </div>
            </div>
        );
    }

    toggle = () => {
        const { measuredHeight } = this.state;
        const { minHeight } = this.props;
        if (measuredHeight > minHeight) {
            const { collapsed } = this.state;
            this.setState({collapsed: !collapsed});
            const { onToggle } = this.props;
            onToggle && onToggle(collapsed);
        }
    }
}

CollapsibleContainer.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    onToggle: PropTypes.func,
    minHeight: PropTypes.number,
    decorStyle: PropTypes.any,
};

CollapsibleContainer.defaultProps = {
    minHeight: 100,
};