import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import './ReviewCard.css';
import HcCard from '../HcCard/HcCard';
import HcButton from '../HcButton/HcButton';
import UserAvatar from '../UserAvatar/UserAvatar';
import Rating from '../Rating/Rating';
import Thumbs from '../Thumbs/Thumbs';
import CollapsibleContainer from '../CollapsibleContainer/CollapsibleContainer';
import CommentContainer from '../../containers/CommentContainer';

export default class ReviewCard extends Component {
    state={
        rate: 0,
        like: null,
        newComment: null,
        editMode: false,
    }
    render() {
        return(
            <HcCard
            style={{
                margin: '100px 0',
            }}>
                <div className="hc-user-review-card__header">
                    <UserAvatar
                        imgSrc={this.props.authorImage}
                        name={this.props.authorName}
                        description={new Date(this.props.creationDate).toLocaleDateString()}
                        label={'Add comment'}
                    />
                </div>
                <div className="hc-user-review-card__body">
                    <div>
                        <h1>{this.props.title}</h1>
                    </div>
                    {this.renderRateSection()}
                    {this.renderContent()}
                    {this.renderCommentSection()}
                </div>
            </HcCard>
        );
    }

    renderRateSection() {
        return(
            <div
            style={{
                display: 'flex',
                margin: '20px auto',
                height: '50px',
                alignItems: 'center',
            }}>
                <Thumbs
                    style={{
                        height: '100%',
                    }}
                    value={this.props.liked}
                />
                <div
                    style={{
                        display: 'inline-flex',
                        height: '100%',
                        border: '1px solid rgb(223, 226, 230)',
                        alignItems: 'center',
                        padding: '0 15px'
                    }}>
                    <Rating
                        rate={this.props.rating}
                        range={6}
                    />
                </div>
            </div>
        );
    }

    renderContent() {
        return (
            <CollapsibleContainer>
                <div>
                    <p>{this.props.content}</p>
                </div>
            </CollapsibleContainer>
        );
    }

    renderCommentSection() {
        const commentValue = (this.props.relatedComment && this.props.relatedComment.content) || this.state.newComment;
        if (typeof commentValue === 'string') {
            return(
                <div style={{margin: '40px 0', width: '100%'}}>
                    <CommentContainer
                        comment={commentValue}
                        user={this.props.relatedComment && this.props.relatedComment.author}
                        editMode={this.state.editMode}
                        onCommentSubmit={this.onCommentSubmit}
                        onSwitchEditMode={() => this.setState({editMode: true})}/>
                </div>
            );
        }
        return(
            <div
                style={{
                    margin: '40px auto',
                    display: 'flex',
                    justifyContent: 'center',
                }}>
                <HcButton label={'Add comment'} onClick={this.startAddingNewComment}/>
            </div>
        );
    }

    onCommentSubmit = (value) => {
        this.setState({
            editMode: false,
            newComment: value,
        });
        const { onCommentSubmit } = this.props;
        onCommentSubmit && onCommentSubmit(value);
    }

    startAddingNewComment = () => {
        this.setState({
            newComment: '',
            editMode: true,
        });
    }
}

ReviewCard.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    title: PropTypes.string,
    content: PropTypes.string,
    creationDate: PropTypes.any,
    relatedComment: PropTypes.object,
    onCommentSubmit: PropTypes.func,
    rating: PropTypes.number,
    liked: PropTypes.bool,
    authorName: PropTypes.string,
    authorImage: PropTypes.any,
};

ReviewCard.defaultProps = {
    authorImage: 'https://randomuser.me/api/portraits/men/97.jpg',
    title: 'Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
};
