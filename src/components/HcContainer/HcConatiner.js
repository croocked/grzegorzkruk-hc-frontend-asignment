import React from 'react';
import sizeMe from 'react-sizeme';
import { PropTypes } from 'prop-types';

const HcContainer = (props) => {
    return (
        <div
        className={props.className}>
            {props.children}
        </div>
    );
};
HcContainer.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    className: PropTypes.string,
};

export default sizeMe()(HcContainer);