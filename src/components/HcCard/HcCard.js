import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './HcCard.css';

export default class HcCard extends Component {
    render() {
        return(
            <div
            style={this.props.style}
            className="hc-card">
                {this.props.children}
            </div>
        );
    }
}

HcCard.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    style: PropTypes.any
};