import React, { Component } from 'react';
import './Thumbs.css';
import { PropTypes } from 'prop-types';

const ThumbButton = (props) => {
    const iconClassUp = 'icon-thumbs-up-alt';
    const iconClassDown = 'icon-thumbs-down-alt';

    const { isLiked, onClick } = props;

    const containerClasses = [
        'hc-thumb-button',
        (isLiked !== null) && `${(isLiked === true) ? 'hc-thumb-button--up' : 'hc-thumb-button--down'}`,
    ].join(' ');

    const iconClasses = [
        (isLiked === false) ? iconClassDown : iconClassUp,
    ].join(' ');

    return (
        <div
        className={containerClasses}
        onClick={onClick}>
            <i className={iconClasses}/>
        </div>
    );
};

ThumbButton.propTypes = {
    isLiked: PropTypes.bool,
    onClick: PropTypes.func,
};

export default class Thumbs extends Component {
    state = {
        isOpen: false,
    }

    render() {
        const { isOpen } = this.state;
        return (
            <div
            className="hc-thumb"
            style={this.props.style}>
                <div
                className="hc-thumb-button__wrapper"
                style={{
                    zIndex: 3,
                }}>
                    <ThumbButton
                    isLiked={this.props.value}
                    onClick={this.toggle}
                    />
                </div>
                {(this.props.value === null || this.props.value === false) &&
                    <div
                    className="hc-thumb-button__wrapper"
                    style={{
                        top: `${isOpen ? '-100%' : 0}`,
                    }}>
                        <ThumbButton
                        isLiked={true}
                        onClick={() => this.setValue(true)}
                        />
                    </div>
                }
                {(this.props.value === null || this.props.value === true) &&
                    <div
                    className="hc-thumb-button__wrapper"
                    style={{
                        top: `${isOpen ? '100%' : 0}`,
                    }}>
                        <ThumbButton
                        isLiked={false}
                        onClick={() => this.setValue(false)}
                        />
                    </div>
                }
            </div>
        );
    }

    setValue = (initValue) => {
        const {
            value,
            onChange,
        } = this.props;

        if (onChange) {
            (value === null) ? onChange(initValue) : onChange(!value);
        }
        this.toggle();
    }

    toggle = () => {
        const { isOpen } = this.state;
        this.setState({
            isOpen: !isOpen
        });
    }
}

Thumbs.propTypes = {
    style: PropTypes.any,
    value: PropTypes.bool,
    onChange: PropTypes.func,
};

Thumbs.defaultProps = {
    style: {},
    value: null,
    onChange: () => { return; },
};