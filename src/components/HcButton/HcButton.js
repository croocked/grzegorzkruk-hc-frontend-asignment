import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import './HcButton.css';

export default class HcButton extends Component {
    render() {
        return (
            <button
            type="button"
            className="hc-button"
            onClick={this.props.onClick}>
                <span>{this.props.label}</span>
            </button>
        );
    }
}

HcButton.propTypes = {
    label: PropTypes.string,
    onClick: PropTypes.func,
};